<?php

$conn = new PDO('mysql:host=localhost;dbname=dbname', "dbuser", "dbpass");

$stmt = $conn->prepare('INSERT INTO `dbname`.`pessoas` (`nome`, `sobrenome`, `idade`) VALUES
(:nome, :sobrenome, :idade)');
$stmt->bindValue(':nome', $_POST['nome']);
$stmt->bindValue(':sobrenome', $_POST['sobrenome']);
$stmt->bindValue(':idade', $_POST['idade']);
$stmt->execute();

header("location:/index.php");

?>
