<?php
namespace Deployer;

require 'recipe/common.php';

// Project name
set('application', 'my_project');

// Project repository
set('repository', 'https://gitlab.com/EstevanLJ/app-gerencia.git');
set('repository_tar', 'https://gitlab.com/EstevanLJ/app-gerencia/-/archive/master/app-gerencia-master.tar.gz');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true); 

// Shared files/dirs between deploys 
set('shared_files', []);
set('shared_dirs', []);

// Writable dirs by web server 
set('writable_dirs', []);
set('allow_anonymous_stats', false);

// Hosts

//VM Puppet
host('127.0.0.1')
    ->set('deploy_path', '/home/vagrant/app')
    ->stage('puppet')
    ->port(2222)
    ->user('vagrant')
    ->identityFile('/home/estevan.junges/ambientes-gerencia/vm_puppet/.vagrant/machines/default/virtualbox/private_key')
    ->forwardAgent(true)
    ->multiplexing(true)
    ->addSshOption('StrictHostKeyChecking', 'no');    

//VM Shell
host('127.0.0.1')
    ->set('deploy_path', '/home/vagrant/app')
    ->stage('shell')
    ->port(2222)
    ->user('vagrant')
    ->identityFile('/home/estevan.junges/ambientes-gerencia/vm_shell/.vagrant/machines/default/virtualbox/private_key')
    ->forwardAgent(true)
    ->multiplexing(true)
    ->addSshOption('StrictHostKeyChecking', 'no');  
    

// Custom Tasks

desc('Roda o script de create tables');
task('create_tables', function () {
    run('mysql -u dbuser -pdbpass dbname < /home/vagrant/app/current/database.sql');
});

desc('Baixa o tar.gz e extrai os arquivos');
task('update_code_tar_gz', function () {
    $repository = get('repository_tar');
    run("wget --quiet $repository -O /tmp/last.tar.gz");
    run("tar -C {{release_path}} -zxvf /tmp/last.tar.gz app-gerencia-master");
});

desc('Deploy your project');
task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
    'success'
]);

desc('Deploy your tar.gz project');
task('deploy_tar', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'update_code_tar_gz',
    'deploy:shared',
    'deploy:writable',
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
    'success'
]);

// [Optional] If deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');
