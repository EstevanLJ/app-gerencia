CREATE TABLE `dbname`.`pessoas` ( 
	`id` INT NOT NULL AUTO_INCREMENT , 
	`nome` VARCHAR(255) NOT NULL , 
	`sobrenome` VARCHAR(255) NOT NULL , 
	`idade` INT(11) NOT NULL , PRIMARY KEY (`id`)
) ENGINE = InnoDB;

INSERT INTO `dbname`.`pessoas` (`nome`, `sobrenome`, `idade`) VALUES
('Joao', 'da Silva', 25),
('Maria', 'Aparecida', 28);