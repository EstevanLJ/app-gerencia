<?php
$conn = new PDO('mysql:host=localhost;dbname=dbname', "dbuser", "dbpass");

$stmt = $conn->prepare('SELECT * FROM pessoas');
$stmt->execute();

?>


<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

    <title>Index</title>
  </head>
  <body>

    <div class="container-fluid">


        <h1>Bem vindo!</h1>

        <hr>

        <h3>Cadastre uma pessoa</h3>

        <form method="POST" action="/cadastrar.php">
          <div class="form-group">
            <label>Nome</label>
            <input type="text" name="nome" class="form-control">
          </div>

          <div class="form-group">
            <label>Sobrenome</label>
            <input type="text" name="sobrenome" class="form-control">
          </div>

          <div class="form-group">
            <label>Idade</label>
            <input type="text" name="idade" class="form-control">
          </div>

          <button type="submit" class="btn btn-primary">Cadastrar</button>
        </form>

        <hr>

        <h3>Lista de pessoas</h3>

        <table class="table">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Nome</th>
              <th scope="col">Sobrenome</th>
              <th scope="col">Idade</th>
            </tr>
          </thead>
          <tbody>

            <?php
                while($pessoa = $stmt->fetch(PDO::FETCH_OBJ)) {
                    echo('<tr>
                      <th scope="row">' . $pessoa->id . '</th>
                      <td>' . $pessoa->nome . '</td>
                      <td>' . $pessoa->sobrenome . '</td>
                      <td>' . $pessoa->idade . '</td>
                    </tr>');
                }
            ?>

          </tbody>
        </table>

    <div>


  </body>
</html>